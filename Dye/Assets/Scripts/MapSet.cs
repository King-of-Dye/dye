using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[CreateAssetMenu(fileName = "New Map Set", menuName = "Rounds/Map Set")]
public class MapSet : ScriptableObject
{
    [Scene] [SerializeField] private List<string> m_maps = new List<string>();

    public IReadOnlyCollection<string> m_Maps => m_maps.AsReadOnly();
}
