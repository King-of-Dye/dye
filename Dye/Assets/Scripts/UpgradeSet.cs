using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Upgrades/New Upgrade Set")]
public class UpgradeSet : ScriptableObject
{
    public int m_requiredScore = 0;
    public List<Upgrade> m_Upgrades = new List<Upgrade>();
}
