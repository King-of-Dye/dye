using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using Mirror;

public class SteamLobby : MonoBehaviour
{
    [SerializeField] private GameObject m_landingPanel = null;

    protected Callback<LobbyCreated_t> LobbyCreated;
    protected Callback<GameLobbyJoinRequested_t> GameLobbyJoinRequested;
    protected Callback<LobbyEnter_t> LobbyEntered;

    private const string HostAddressKey = "HostAddress";

    public static CSteamID LobbyID { get; private set; }

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    private void Start()
    {
        if (!SteamManager.Initialized)
            return;

        LobbyCreated = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
        GameLobbyJoinRequested = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
        LobbyEntered = Callback<LobbyEnter_t>.Create(OnLobbyEntered);
    }

    public void HostLobby()
    {
        m_landingPanel.SetActive(false);

        SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, Room.maxConnections);
    }

    private void OnLobbyCreated(LobbyCreated_t _callback)
    {
        if (_callback.m_eResult != EResult.k_EResultOK)
        {
            m_landingPanel.SetActive(true);
            return;
        }

        LobbyID = new CSteamID(_callback.m_ulSteamIDLobby);

        Room.StartHost();

        SteamMatchmaking.SetLobbyData(LobbyID, HostAddressKey, SteamUser.GetSteamID().ToString());
    }

    private void OnGameLobbyJoinRequested(GameLobbyJoinRequested_t _callback)
    {
        SteamMatchmaking.JoinLobby(_callback.m_steamIDLobby);
    }

    private void OnLobbyEntered(LobbyEnter_t _callback)
    {
        if (NetworkServer.active)
            return;

        string hostAddress = SteamMatchmaking.GetLobbyData(new CSteamID(_callback.m_ulSteamIDLobby), HostAddressKey);

        Room.networkAddress = hostAddress;
        Room.StartClient();

        m_landingPanel.SetActive(false);
    }
}
