using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using TMPro;
using UnityEngine.UI;
using Steamworks;

public class RoomPlayerDye : NetworkBehaviour
{
    [Header("UI")]
    [SerializeField] private GameObject m_lobbyUI = null;
    [SerializeField] private TMP_Text[] m_playerNameTexts = new TMP_Text[4];
    [SerializeField] private TMP_Text[] m_playerReadyTexts = new TMP_Text[4];
    [SerializeField] private RawImage[] m_playerIcons = new RawImage[4];
    [SerializeField] private Image[] m_playerColorsDisplay = new Image[4];
    [SerializeField] private Button m_startGameButton = null;
    [SerializeField] private Button m_inviteFriendButton = null;

    [SyncVar(hook = nameof(HandleSteamIDUpdated))]
    private ulong m_steamId;

    [SyncVar(hook = nameof(HandleDisplayNameChanged))]
    public string m_DisplayName = "Loading...";
    public Texture2D m_ProfileImage = null;
    [SyncVar(hook = nameof(HandleReadyStatusChanged))]
    public bool m_IsReady;

    [SyncVar] public PlayerPrefabs m_PlayerPrefabs = null;

    private bool m_isLobbyLeader;
    public bool IsLobbyLeader
    {
        set
        {
            m_isLobbyLeader = value;
            m_startGameButton.gameObject.SetActive(value);
            m_inviteFriendButton.gameObject.SetActive(value);
        }
    }

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    protected Callback<AvatarImageLoaded_t> AvatarImageLoaded;
    protected Callback<LobbyChatUpdate_t> LobbyChatUpdated;

    public override void OnStartAuthority()
    {
        m_lobbyUI.SetActive(true);
    }

    public override void OnStartClient()
    {
        Room.RoomPlayers.Add(this);

        UpdateDisplay();

        AvatarImageLoaded = Callback<AvatarImageLoaded_t>.Create(OnAvatarImageLoaded);
        LobbyChatUpdated = Callback<LobbyChatUpdate_t>.Create(OnLobbyChatUpdate);
    }

    private void OnLobbyChatUpdate(LobbyChatUpdate_t callback)
    {
        Debug.Log("Callback");
        UpdateDisplay();
    }

    public override void OnStopClient()
    {
        Room.RoomPlayers.Remove(this);

        UpdateDisplay();
    }

    public void HandleReadyStatusChanged(bool _oldBool, bool _newBool)
    {
        UpdateDisplay();
    }

    public void HandleDisplayNameChanged(string _oldString, string _newString)
    {
        UpdateDisplay();
    }

    private void HandleSteamIDUpdated(ulong _oldId, ulong _newId)
    {
        CSteamID cSteamId = new CSteamID(_newId);
        m_DisplayName = SteamFriends.GetFriendPersonaName(cSteamId);

        int imageId = SteamFriends.GetLargeFriendAvatar(cSteamId);

        if (imageId == -1)
            return;

        m_ProfileImage = GetSteamImageAsTexture(imageId);

        UpdateDisplay();
    }

    private void OnAvatarImageLoaded(AvatarImageLoaded_t _callback)
    {
        if (_callback.m_steamID.m_SteamID != m_steamId)
            return;

        m_ProfileImage = GetSteamImageAsTexture(_callback.m_iImage);

        UpdateDisplay();
    }

    private Texture2D GetSteamImageAsTexture(int iImage)
    {
        Texture2D texture = null;

        bool isValid = SteamUtils.GetImageSize(iImage, out uint width, out uint height);

        if (isValid)
        {
            byte[] image = new byte[width * height * 4];

            isValid = SteamUtils.GetImageRGBA(iImage, image, (int)(width * height * 4));

            if (isValid)
            {
                texture = new Texture2D((int)width, (int)height, TextureFormat.RGBA32, false, true);
                texture.LoadRawTextureData(image);
                texture.Apply();
            }
        }

        return texture;
    }

    private void UpdateDisplay()
    {
        if (!hasAuthority)
        {
            foreach (RoomPlayerDye player in Room.RoomPlayers)
            {
                if (player.hasAuthority)
                {
                    player.UpdateDisplay();
                    break;
                }
            }

            return;
        }

        for (int i = 0; i < m_playerNameTexts.Length; i++)
        {
            if (m_playerNameTexts[i] != null)
            {
                m_playerNameTexts[i].text = "Waiting For Player...";
                m_playerReadyTexts[i].text = string.Empty;
            }
            if (m_playerIcons[i] != null)
            {
                m_playerIcons[i].texture = null;
                m_playerIcons[i].enabled = false;
            }
            //Better Icon than white square
            if (m_playerColorsDisplay[i] != null)
            {
                m_playerColorsDisplay[i].sprite = null;
                m_playerColorsDisplay[i].enabled = false;
            }
        }

        for (int i = 0; i < Room.RoomPlayers.Count; i++)
        {
            m_playerNameTexts[i].text = Room.RoomPlayers[i].m_DisplayName;
            m_playerReadyTexts[i].text = Room.RoomPlayers[i].m_IsReady ?
                "<color=green>Ready</color>" :
                "<color=red>Not Ready</color>";
            m_playerIcons[i].enabled = true;
            m_playerIcons[i].texture = Room.RoomPlayers[i].m_ProfileImage;

            m_playerColorsDisplay[i].enabled = true;
        }
    }

    public void HandleReadyToStart(bool _ready)
    {
        if (!m_isLobbyLeader)
            return;

        m_startGameButton.interactable = _ready;
    }

    [Command]
    public void CmdReadyUp()
    {
        m_IsReady = !m_IsReady;

        Room.NotifyPlayersOfReadyState();
    }

    [Command]
    public void CmdStartGame()
    {
        if (Room.RoomPlayers[0].connectionToClient != connectionToClient)
            return;

        Room.StartGame();
    }

    public void SetSteamId(ulong _steamID)
    {
        this.m_steamId = _steamID;
    }

    public void SetPlayerPrefabs(PlayerPrefabs _playerPrefabs)
    {
        this.m_PlayerPrefabs = _playerPrefabs;

        UpdateDisplay();
    }

    public void ActivateGameOverlayInviteDialog()
    {
        SteamFriends.ActivateGameOverlayInviteDialog(SteamLobby.LobbyID);
    }

    public void LeaveLobby()
    {
        SteamMatchmaking.LeaveLobby(SteamLobby.LobbyID);

        if (m_isLobbyLeader)
        {
            Room.StopHost();
        }
        else
        {
            Room.StopClient();
        }
    }

    private void OnDestroy()
    {
        SteamMatchmaking.LeaveLobby(SteamLobby.LobbyID);
    }
}
