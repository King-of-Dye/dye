using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreBoard : MonoBehaviour
{
    [SerializeField] private Image[] m_colorDrops = new Image[5];
    [SerializeField] private Image[] m_colorBuckets = new Image[5];

    public void UpdateScoreUI(int _score)
    {
        DeactivateAllImages();

        switch (_score)
        {
            case 1:
                m_colorDrops[0].enabled = true;
                break;
            case 2:
                m_colorBuckets[0].enabled = true;
                break;
            case 3:
                m_colorDrops[1].enabled = true;
                m_colorBuckets[0].enabled = true;
                break;
            case 4:
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[0].enabled = true;
                break;
            case 5:
                m_colorDrops[2].enabled = true;
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                break;
            case 6:
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[2].enabled = true;
                break;
            case 7:
                m_colorDrops[3].enabled = true;
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[2].enabled = true;
                break;
            case 8:
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[2].enabled = true;
                m_colorBuckets[3].enabled = true;
                break;
            case 9:
                m_colorDrops[4].enabled = true;
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[2].enabled = true;
                m_colorBuckets[3].enabled = true;
                break;
            case 10:
                m_colorBuckets[0].enabled = true;
                m_colorBuckets[1].enabled = true;
                m_colorBuckets[2].enabled = true;
                m_colorBuckets[3].enabled = true;
                m_colorBuckets[4].enabled = true;
                break;
            default:
                break;
        }
    }

    private void DeactivateAllImages()
    {
        m_colorDrops[0].enabled = false;
        m_colorDrops[1].enabled = false;
        m_colorDrops[2].enabled = false;
        m_colorDrops[3].enabled = false;
        m_colorDrops[4].enabled = false;

        m_colorBuckets[0].enabled = false;
        m_colorBuckets[1].enabled = false;
        m_colorBuckets[2].enabled = false;
        m_colorBuckets[3].enabled = false;
        m_colorBuckets[4].enabled = false;
    }
}
