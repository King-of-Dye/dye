using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundOptions : MonoBehaviour
{
    [Header("Audio Mixer")]
    [SerializeField] private AudioMixer m_audioMixer;

    [Header("SoundMenu")]
    [SerializeField] private Slider m_masterSlider;
    [SerializeField] private Slider m_musicSlider;
    [SerializeField] private Slider m_soundFXSlider;
    [SerializeField] private Toggle m_muteToggle;

    [Header("AudioSources")]
    [SerializeField] private AudioSource m_soundFxAudioSource;
    [SerializeField] private AudioSource m_musicAudioSource;

    [Header("SoundFX")]
    [SerializeField] private AudioClip m_backAudioClip;
    [SerializeField] private AudioClip m_GoFurtherClip;
    [SerializeField] private AudioClip m_UpgradeClip;
    [SerializeField] private AudioClip m_PlayClip;
    [SerializeField] private AudioClip m_HitClip;

    [Header("Soundtracks")]
    [SerializeField] private AudioClip m_menuTrack;
    [SerializeField] private AudioClip m_upgradeTrack;
    [SerializeField] private AudioClip m_endScreenTrack;

    [Header("Map Soundtracks")]
    [SerializeField] private AudioClip m_Map1Track;
    [SerializeField] private AudioClip m_Map2Track;
    [SerializeField] private AudioClip m_Map3Track;
    [SerializeField] private AudioClip m_Map4Track;
    [SerializeField] private AudioClip m_Map5Track;
    [SerializeField] private AudioClip m_Map6Track;

    [Header("SoundValues")]
    private float m_masterVol;
    private float m_musicVol;
    private float m_soundFXVol;

    private float m_muteTemp;


    private void Awake()
    {
        SceneManager.activeSceneChanged += PlaySoundtrack;
    }

    void Start()
    {
        m_masterVol = PlayerPrefs.GetFloat("MasterVol", 1f);
        m_musicVol = PlayerPrefs.GetFloat("MusicVol", 1f);
        m_soundFXVol = PlayerPrefs.GetFloat("SoundFXVol", 1f);

        SetMaster(m_masterVol);
        SetMusic(m_musicVol);
        SetSoundFX(m_soundFXVol);

    }

    #region Audio Settings

    public void MuteAudio(Toggle _toggle)
    {
        if (!_toggle.isOn)
        {
            m_masterSlider.interactable = true;
            m_masterSlider.value = m_muteTemp;
            m_audioMixer.SetFloat("Master", Mathf.Log10(m_muteTemp) * 20);
        }
        else if (_toggle.isOn)
        {
            m_muteTemp = m_masterSlider.value;
            m_audioMixer.SetFloat("Master", Mathf.Log10(m_masterSlider.minValue)* 20);

            m_masterSlider.value = m_masterSlider.minValue;
            m_masterSlider.interactable = false;
        }
    }

    public void SetMaster()
    {
        m_audioMixer.SetFloat("Master", Mathf.Log10(m_masterSlider.value) * 20);
        PlayerPrefs.SetFloat("MasterVol", m_masterSlider.value);
    }
    
    public void SetMaster(float _value)
    {
        m_audioMixer.SetFloat("Master", Mathf.Log10(_value) * 20);
        m_masterSlider.value = _value;
    }

    public void SetMusic()
    {
        m_audioMixer.SetFloat("Music", Mathf.Log10(m_musicSlider.value) * 20);
        PlayerPrefs.SetFloat("MusicVol", m_musicSlider.value);
    }

    public void SetMusic(float _value)
    {
        m_audioMixer.SetFloat("Music", Mathf.Log10(_value) * 20);
        m_musicSlider.value = _value;
    }

    public void SetSoundFX()
    {
        m_audioMixer.SetFloat("SoundFX", Mathf.Log10(m_soundFXSlider.value) * 20);
        PlayerPrefs.SetFloat("SoundFXVol", m_soundFXSlider.value);
    }
    public void SetSoundFX(float _value)
    {
        m_audioMixer.SetFloat("SoundFX", Mathf.Log10(_value) * 20);
        m_soundFXSlider.value = _value;
    }

    #endregion


    #region Button Sounds
    public void BackSound()
    {
        m_soundFxAudioSource.PlayOneShot(m_backAudioClip);
    }

    public void GoFurther()
    {
        m_soundFxAudioSource.PlayOneShot(m_GoFurtherClip);
    }

    public void UpgradeSound()
    {
        m_soundFxAudioSource.PlayOneShot(m_UpgradeClip);
    }

    public void PlaySound()
    {
        m_soundFxAudioSource.PlayOneShot(m_PlayClip);
    }

    #endregion

    private void PlaySoundtrack(Scene current, Scene next)
    {
        string sceneName = SceneManager.GetActiveScene().name;


        switch (sceneName)
        {
            case "Scene_MainMenu_01":
                m_musicAudioSource.clip = m_menuTrack;
                break;

            case "Scene_Updgrade_01":
                m_musicAudioSource.clip = m_upgradeTrack;
                break;

            case "Scene_Map_01":
                m_musicAudioSource.clip = m_Map1Track;
                break;

            case "Scene_Map_02":
                m_musicAudioSource.clip = m_Map2Track;
                break;

            case "Scene_Map_03":
                m_musicAudioSource.clip = m_Map3Track;
                break;

            case "Scene_Map_04":
                m_musicAudioSource.clip = m_Map4Track;
                break;

            case "Scene_Map_05":
                m_musicAudioSource.clip = m_Map5Track;
                break;

            case "Scene_Map_06":
                m_musicAudioSource.clip = m_Map6Track;
                break;

            case "Scene_EndScreen_01":
                m_musicAudioSource.clip = m_endScreenTrack;
                break;

            default:
                break;
        }
        m_musicAudioSource.Play();
    }

    public void PlayHitSound()
    {
        m_soundFxAudioSource.clip =m_HitClip;
        m_soundFxAudioSource.Play();
    }

    private void OnDestroy()
    {
        SceneManager.activeSceneChanged -= PlaySoundtrack;
    }

}
