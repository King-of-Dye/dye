using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class LobbySound : MonoBehaviour
{
    [Header("Audio Mixer & Audio Source")]
    [SerializeField] private AudioMixer m_audioMixer;
    [SerializeField] private AudioSource m_soundFXSource;
    [SerializeField] private AudioSource m_musicSource;

    [Header("SoundFX")]
    [SerializeField] private AudioClip m_backAudioClip;
    [SerializeField] private AudioClip m_GoFurtherClip;

    [Header("Soundtrack")]
    [SerializeField] private AudioClip m_lobbyTrack;

    [Header("SoundValues")]
    [SerializeField] private float m_masterVol;
    [SerializeField] private float m_musicVol;
    [SerializeField] private float m_soundFXVol;

    void Start()
    {
        m_masterVol = PlayerPrefs.GetFloat("MasterVol", 1f);
        m_musicVol = PlayerPrefs.GetFloat("MusicVol", 1f);
        m_soundFXVol = PlayerPrefs.GetFloat("SoundFXVol", 1f);

        SetSoundVolume();
        PlaySoundtrack();
    }

    public void SetSoundVolume()
    {
        m_audioMixer.SetFloat("Master", Mathf.Log10(m_masterVol) * 20);
        m_audioMixer.SetFloat("Music", Mathf.Log10(m_musicVol) * 20);
        m_audioMixer.SetFloat("SoundFX", Mathf.Log10(m_soundFXVol) * 20);
    }

    public void BackSound()
    {
        m_soundFXSource.PlayOneShot(m_backAudioClip);
    }

    public void GoFurther()
    {
        m_soundFXSource.PlayOneShot(m_GoFurtherClip);
    }

    public void PlaySoundtrack()
    {
        m_musicSource.clip = m_lobbyTrack;
        m_musicSource.Play();
    }
}
