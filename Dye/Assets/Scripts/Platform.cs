using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Platform : NetworkBehaviour
{
    [SerializeField] private Renderer m_modelRenderer = null;

    [SerializeField] private int m_bulletsRequired;

    private Dictionary<Color, int> m_bulletCount = new Dictionary<Color, int>();

    [SyncVar(hook = nameof(HandlePlatformColorChanged))] private Color m_platformColor = Color.black;

    public GameObject m_PaintParent = null;

    private Color m_newYellow = new Color(1, 1, 0, 1);

    private void Start()
    {
        m_bulletCount.Add(Color.red, 0);
        m_bulletCount.Add(Color.blue, 0);
        m_bulletCount.Add(Color.green, 0);
        m_bulletCount.Add(m_newYellow, 0);
    }

    [Server]
    public void PlatformHit(Bullet _bullet)
    {
        if (_bullet.m_BulletColor == Color.red)
        {
            m_bulletCount[Color.red]++;
            m_bulletCount[Color.blue] = Mathf.Max(m_bulletCount[Color.blue]--, 0);
            m_bulletCount[Color.green] = Mathf.Max(m_bulletCount[Color.green]--, 0);
            m_bulletCount[m_newYellow] = Mathf.Max(m_bulletCount[m_newYellow]--, 0);
        }
        else if(_bullet.m_BulletColor == Color.blue)
        {
            m_bulletCount[Color.red] = Mathf.Max(m_bulletCount[Color.red]--, 0);
            m_bulletCount[Color.blue]++;
            m_bulletCount[Color.green] = Mathf.Max(m_bulletCount[Color.green]--, 0);
            m_bulletCount[m_newYellow] = Mathf.Max(m_bulletCount[m_newYellow]--, 0);
        }
        else if (_bullet.m_BulletColor == Color.green)
        {
            m_bulletCount[Color.red] = Mathf.Max(m_bulletCount[Color.red]--, 0);
            m_bulletCount[Color.blue] = Mathf.Max(m_bulletCount[Color.blue]--, 0);
            m_bulletCount[Color.green]++;
            m_bulletCount[m_newYellow] = Mathf.Max(m_bulletCount[m_newYellow]--, 0);
        }
        else if (_bullet.m_BulletColor == m_newYellow)
        {
            m_bulletCount[Color.red] = Mathf.Max(m_bulletCount[Color.red]--, 0);
            m_bulletCount[Color.blue] = Mathf.Max(m_bulletCount[Color.blue]--, 0);
            m_bulletCount[Color.green] = Mathf.Max(m_bulletCount[Color.green]--, 0);
            m_bulletCount[m_newYellow]++;
        }

        int highestValue = 0;
        Color highestValueColor = Color.black;

        foreach (KeyValuePair<Color, int> color in m_bulletCount)
        {
            if(color.Value > highestValue)
            {
                highestValue = color.Value;
                highestValueColor = color.Key;
            }
        }

        if(highestValue >= m_bulletsRequired)
        {
            m_platformColor = highestValueColor;
        }
        else
        {
            m_platformColor = Color.black;
        }
    }

    public void HandlePlatformColorChanged(Color _oldColor, Color _newColor)
    {
        m_modelRenderer.materials[0].color = _newColor;
        m_modelRenderer.materials[0].SetColor("_EmissionColor", _newColor * 5f);
    }

    public Color GetPlatformColor()
    {
        return m_platformColor;
    }
}
