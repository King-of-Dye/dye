using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class PlayerRoundControllerDye : NetworkBehaviour
{
    [Header("References")]
    private Rigidbody m_rigidBody;

    [SyncVar] public GamePlayerDye m_MyGamePlayer;
    public GameObject m_PlayerHUD;
    public PlayerRoundControllerUI m_PlayerUI;

    [Header("Stats")]
    [SyncVar] public int m_MaxHealth = 100;
    [SyncVar(hook = nameof(OnCurrentHealthUpdated))] public int m_CurrentHealth;
    [SyncVar] public int m_HealthRegen = 0;
    private float m_regenTimer = 0;

    [Header("Movement Stats")]
    [SyncVar] public float m_MovementSpeed;

    [SyncVar] public float m_JumpSpeed;
    [SyncVar] public int m_Jumps;
    [SyncVar] public int m_JumpsLeft;
    [SyncVar] public bool m_IsGrounded = false;
    [SyncVar] private Vector3 m_impactVector;
    [SyncVar] private float m_impactVectorForce = 0;
    public LayerMask m_GroundMask;

    [Header("Shooting")]
    [SyncVar] public Vector3 m_AimDirection;
    public Transform m_WeaponMuzzle;
    public Transform m_WeaponModel;
    public Transform m_WeaponPivot;
    [SyncVar] public int m_CurrentAmmo;
    [SyncVar] public int m_BaseAmmo = 3;
    [SyncVar] public int m_Damage = 30;
    [SyncVar] public float m_SplashDamage = 0;
    [SyncVar] public float m_SplashRadius = 0;
    [SyncVar] public float m_BulletMass = 1f;
    [SyncVar] public bool m_BulletGravity = true;
    [SyncVar] public float m_BulletSpeed = 25f;
    [SyncVar] public float m_ReloadSpeed = 1.5f;
    [SyncVar] public float m_AttackSpeed = 0.3f;
    [SyncVar] public bool m_CanShoot = true;
    [SyncVar] public bool m_Reloading = false;

    private float m_platformDamageTimer = 0;
    private float m_nextPlatformDamageTick = 1;

    public static event Action<NetworkConnection> OnPlayerDeath;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    #region Callbacks

    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
    }

    public override void OnStartAuthority()
    {
        RoundSystem.OnCountdownStarted += CmdUpdatePlayerStatsAndColor;
        RoundSystem.OnRoundStarted += EnableThisScript;
    }

    public override void OnStartClient()
    {
        m_CurrentHealth = m_MaxHealth;
        m_CurrentAmmo = m_BaseAmmo;
    }

    private void OnDisable()
    {
        RoundSystem.OnRoundStarted -= EnableThisScript;
        RoundSystem.OnCountdownStarted -= CmdUpdatePlayerStatsAndColor;
    }

    #endregion Callbacks

    #region Unity Methodes

    private void Start()
    {
        //Maybe Activate the correct Color HUD

        m_PlayerHUD.SetActive(true);
    }

    private void Update()
    {
        Jump();
        Aim();
        Shoot();
    }

    private void FixedUpdate()
    {
        Movement();
        CheckForColoredGround();
        CheckIfOutsideMap();

        if (m_impactVectorForce > 0)
        {
            m_impactVectorForce -= 2 * Time.fixedDeltaTime;
        }

        if (m_HealthRegen > 0)
        {
            if (m_regenTimer <= 0)
            {
                CmdHealMe(m_HealthRegen);
                m_regenTimer = 1;
            }
            else
            {
                m_regenTimer -= Time.deltaTime;
            }
        }
    }
    #endregion Unity Updates

    private void EnableThisScript()
    {
        enabled = true;
    }

    private void OnCurrentHealthUpdated(int _oldValue, int _newValue)
    {
        m_PlayerUI.UpdateHealthBar(m_MaxHealth, _newValue);
    }

    private void Movement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");

        Vector3 velocity = (new Vector3(horizontal * (m_MovementSpeed * transform.localScale.x), m_rigidBody.velocity.y, 0)) + (m_impactVector * m_impactVectorForce);

        m_rigidBody.velocity = velocity;
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (Physics.CheckSphere(transform.position, 0.3f, m_GroundMask))
            {
                if (!m_IsGrounded)
                {
                    m_IsGrounded = true;
                    m_JumpsLeft = m_Jumps;
                }
            }

            if (m_JumpsLeft > 0)
            {
                m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, m_JumpSpeed * transform.localScale.x, 0);
                m_JumpsLeft--;
                m_IsGrounded = false;
            }
        }
    }

    private void Aim()
    {
        Vector2 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        m_AimDirection = new Vector3(mousePos.x, mousePos.y, 0) - (transform.position + new Vector3(0, 0.5f, 0));
        m_AimDirection.Normalize();

        m_WeaponPivot.transform.right = m_AimDirection;

        if (m_AimDirection.x < 0)
        {
            m_WeaponModel.transform.localRotation = Quaternion.Euler(180, 180, 0);
        }
        else
        {
            m_WeaponModel.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        Debug.DrawLine(transform.position, mousePos);
        CmdUpdateAimDirection(m_AimDirection);
    }

    public void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CmdSpawnBullet();
        }
    }

    private void CheckForColoredGround()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), -transform.up, out hit, 0.75f, m_GroundMask))
        {
            Platform platform = hit.collider.gameObject.GetComponentInParent<Platform>();
            if (platform != null)
            {
                Color platformColor = platform.GetPlatformColor();

                if (platformColor == m_MyGamePlayer.m_PlayerPrefabs.m_PlayerColor || platformColor == Color.black)
                    return;

                m_platformDamageTimer += 1 * Time.fixedDeltaTime;

                if (m_platformDamageTimer >= Mathf.RoundToInt(m_nextPlatformDamageTick))
                {
                    CmdTakeDamage(5);
                    m_nextPlatformDamageTick += 1;
                }
            }
            else
            {
                m_platformDamageTimer = 0;
                m_nextPlatformDamageTick = 1;
            }
        }
    }

    private void CheckIfOutsideMap()
    {
        if (transform.position.y <= -30f)
        {
            CmdTakeDamage(99999);
        }
        else if(transform.position.y > 30f)
        {
            Vector3 temp = (Vector3.zero - transform.position).normalized;
            CmdImpactForce(temp, 1f);
        }
    }

    #region Commands

    [Command]
    public void CmdUpdateAimDirection(Vector3 _direction)
    {
        m_AimDirection = _direction;
    }

    [Command]
    public void CmdUpdatePlayerStatsAndColor()
    {
        UpdatePlayerMaterials();

        if (m_MyGamePlayer.m_UpgradeIDs.Count > 0)
        {
            foreach (int upgradeId in m_MyGamePlayer.m_UpgradeIDs)
            {
                Upgrade tempUpgrade = Room.m_UpgradeDictionary[upgradeId];

                if (tempUpgrade.m_Health != 0)
                {
                    m_MaxHealth += tempUpgrade.m_Health;

                    if (m_MaxHealth <= 0)
                    {
                        m_MaxHealth = 1;
                    }
                }
                if (tempUpgrade.m_HealthMultiply != 0)
                {
                    m_MaxHealth = Mathf.RoundToInt(m_MaxHealth * tempUpgrade.m_HealthMultiply);

                    if (m_MaxHealth <= 0)
                    {
                        m_MaxHealth = 1;
                    }
                }
                if (tempUpgrade.m_HealthRegen != 0)
                {
                    m_HealthRegen += tempUpgrade.m_HealthRegen;

                    if (m_HealthRegen < 0)
                    {
                        m_HealthRegen = 0;
                    }
                }
                if (tempUpgrade.m_JumpSpeed != 0)
                {
                    m_JumpSpeed += tempUpgrade.m_JumpSpeed;

                    if (m_JumpSpeed <= 0)
                    {
                        m_JumpSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_JumpSpeedMultiply != 0)
                {
                    m_JumpSpeed = m_JumpSpeed * tempUpgrade.m_JumpSpeedMultiply;

                    if (m_JumpSpeed <= 0)
                    {
                        m_JumpSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_Jumps != 0)
                {
                    m_Jumps += tempUpgrade.m_Jumps;

                    if (m_Jumps < 0)
                    {
                        m_Jumps = 0;
                    }
                }
                if (tempUpgrade.m_MovementSpeed != 0)
                {
                    m_MovementSpeed += tempUpgrade.m_MovementSpeed;

                    if (m_MovementSpeed <= 0)
                    {
                        m_MovementSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_MovementSpeedMultiply != 0)
                {
                    m_MovementSpeed *= tempUpgrade.m_MovementSpeedMultiply;

                    if (m_MovementSpeed <= 0)
                    {
                        m_MovementSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_Damage != 0)
                {
                    m_Damage += tempUpgrade.m_Damage;

                    if (m_Damage <= 0)
                    {
                        m_Damage = 1;
                    }
                }
                if (tempUpgrade.m_DamageMultiply != 0)
                {
                    m_Damage = Mathf.RoundToInt(m_Damage * tempUpgrade.m_DamageMultiply);

                    if (m_Damage <= 0)
                    {
                        m_Damage = 1;
                    }
                }
                if (tempUpgrade.m_SplashDamage != 0)
                {
                    m_SplashDamage += tempUpgrade.m_SplashDamage;

                    if (m_SplashDamage < 0)
                    {
                        m_SplashDamage = 0;
                    }
                }
                if (tempUpgrade.m_SplashRadius != 0)
                {
                    m_SplashRadius += tempUpgrade.m_SplashRadius;

                    if (m_SplashRadius < 0)
                    {
                        m_SplashRadius = 0;
                    }
                }
                if (tempUpgrade.m_Bullets != 0)
                {
                    m_BaseAmmo += tempUpgrade.m_Bullets;

                    if (m_BaseAmmo <= 1)
                    {
                        m_BaseAmmo = 1;
                    }
                }
                if (tempUpgrade.m_BulletsMultiply != 0)
                {
                    m_BaseAmmo = Mathf.RoundToInt(m_BaseAmmo * tempUpgrade.m_BulletsMultiply);

                    if (m_BaseAmmo <= 1)
                    {
                        m_BaseAmmo = 1;
                    }
                }
                if (tempUpgrade.m_BulletSpeed != 0)
                {
                    m_BulletSpeed += tempUpgrade.m_BulletSpeed;

                    if (m_BulletSpeed <= 0)
                    {
                        m_BulletSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_BulletSpeedMultiply != 0)
                {
                    m_BulletSpeed *= tempUpgrade.m_BulletSpeedMultiply;

                    if (m_BulletSpeed <= 0)
                    {
                        m_BulletSpeed = 0.1f;
                    }
                }
                if (tempUpgrade.m_BulletMass != 0)
                {
                    m_BulletMass += tempUpgrade.m_BulletMass;

                    if (m_BulletMass < 0)
                    {
                        m_BulletMass = 0;
                    }
                }
                if (tempUpgrade.m_ReloadSpeed != 0)
                {
                    m_ReloadSpeed += tempUpgrade.m_ReloadSpeed;

                    if (m_ReloadSpeed <= 0)
                    {
                        m_ReloadSpeed = 0.01f;
                    }
                }
                if (tempUpgrade.m_ReloadMultiply != 0)
                {
                    m_ReloadSpeed *= tempUpgrade.m_ReloadMultiply;

                    if (m_ReloadSpeed <= 0)
                    {
                        m_ReloadSpeed = 0.01f;
                    }
                }
                if (tempUpgrade.m_AttackSpeed != 0)
                {
                    m_AttackSpeed += tempUpgrade.m_AttackSpeed;

                    if (m_AttackSpeed <= 0)
                    {
                        m_AttackSpeed = 0.01f;
                    }
                }
                if (tempUpgrade.m_AttackSpeedMultiply != 0)
                {
                    m_AttackSpeed *= tempUpgrade.m_AttackSpeedMultiply;

                    if (m_AttackSpeed <= 0)
                    {
                        m_AttackSpeed = 0.01f;
                    }
                }

                if (m_BulletGravity == true)
                {
                    m_BulletGravity = tempUpgrade.m_BulletGravity;
                }

                if (tempUpgrade.m_UpgradeModel != null)
                {
                    if (tempUpgrade.m_IsBodyModel)
                    {
                        UpdateBodyUpgradeModel(upgradeId);
                    }
                    else
                    {
                        UpdateWeaponUpgradeModel(upgradeId);
                    }
                }
            }

            m_CurrentHealth = m_MaxHealth;
            m_CurrentAmmo = m_BaseAmmo;

            GameObject instance = Instantiate(m_MyGamePlayer.m_PlayerPrefabs.m_PlayerDeathFX, transform.position, Quaternion.identity);

            NetworkServer.Spawn(instance);
        }
    }

    [Command]
    public void CmdSpawnBullet()
    {
        if (m_CurrentAmmo > 0 && m_CanShoot)
        {
            m_CanShoot = false;

            StartCoroutine(Interclip());

            m_CurrentAmmo--;

            if (m_CurrentAmmo <= 0 && !m_Reloading)
            {
                StartCoroutine(Reloading());
            }

            GameObject instance = Instantiate(m_MyGamePlayer.m_PlayerPrefabs.m_BulletPrefab, this.m_WeaponMuzzle.transform.position, this.m_WeaponMuzzle.transform.rotation);

            Bullet bulletInstance = instance.GetComponent<Bullet>();

            bulletInstance.SetBulletVariables(m_BulletSpeed, m_Damage, m_SplashDamage, m_SplashRadius, m_BulletMass, m_BulletGravity, m_AimDirection, m_MyGamePlayer.m_PlayerColor);

            NetworkServer.Spawn(instance);
        }
        else if(m_CurrentAmmo <= 0 && !m_Reloading)
        {
            StartCoroutine(Reloading());
        }
    }

    [Command]
    public void CmdTakeDamage(int _amount)
    {
        TakeDamage(_amount);
    }

    [Command]
    public void CmdHealMe(int _amount)
    {
        Heal(_amount);
    }

    [Command]
    public void CmdImpactForce(Vector3 _direction, float _force)
    {
        AddImpactVector(_direction * _force);
    }

    #endregion Commands

    #region Server Methodes & RPCs

    [ClientRpc]
    public void UpdatePlayerMaterials()
    {
        transform.GetChild(0).GetComponent<Renderer>().material.color = m_MyGamePlayer.m_PlayerColor;
        m_WeaponModel.GetComponent<Renderer>().materials[1].color = m_MyGamePlayer.m_PlayerColor;
    }

    [ClientRpc]
    public void UpdateBodyUpgradeModel(int _upgradeID)
    {
        Upgrade tempUpgrade = Room.m_UpgradeDictionary[_upgradeID];

        GameObject instance = Instantiate(tempUpgrade.m_UpgradeModel, transform);
        instance.transform.localPosition = tempUpgrade.m_UpgradeModel.transform.position;
        instance.transform.localRotation = tempUpgrade.m_UpgradeModel.transform.rotation;
        instance.transform.localScale = tempUpgrade.m_UpgradeModel.transform.localScale;
    }

    [ClientRpc]
    public void UpdateWeaponUpgradeModel(int _upgradeID)
    {
        Upgrade tempUpgrade = Room.m_UpgradeDictionary[_upgradeID];

        GameObject instance = Instantiate(tempUpgrade.m_UpgradeModel, m_WeaponModel);
        instance.transform.localPosition = tempUpgrade.m_UpgradeModel.transform.position;
        instance.transform.localRotation = tempUpgrade.m_UpgradeModel.transform.rotation;
        instance.transform.localScale = tempUpgrade.m_UpgradeModel.transform.localScale;
    }

    [Server]
    public IEnumerator Reloading()
    {
        m_Reloading = true;

        yield return new WaitForSeconds(m_ReloadSpeed);

        m_CurrentAmmo = m_BaseAmmo;
        m_Reloading = false;
        m_CanShoot = true;
    }

    [Server]
    public IEnumerator Interclip()
    {
        yield return new WaitForSeconds(m_AttackSpeed);

        m_CanShoot = true;
    }

    [Server]
    public void TakeDamage(int _amount)
    {
        m_MyGamePlayer.HitSound();
        m_CurrentHealth = Mathf.Max(m_CurrentHealth - _amount, 0);

        if (m_CurrentHealth <= 0)
        {
            if (m_MyGamePlayer.m_PlayerPrefabs.m_PlayerDeathFX != null)
            {
                GameObject instance = Instantiate(m_MyGamePlayer.m_PlayerPrefabs.m_PlayerDeathFX, transform.position, Quaternion.identity);
                Destroy(instance, 2f);

                NetworkServer.Spawn(instance);
            }

            OnPlayerDeath?.Invoke(this.connectionToClient);

            NetworkServer.Destroy(this.gameObject);
        }
    }

    [Server]
    public void Heal(int _amount)
    {
        m_CurrentHealth = Mathf.Min(m_CurrentHealth + _amount, m_MaxHealth);
    }

    [Server]
    public void AddImpactVector(Vector3 _direction)
    {
        m_impactVector = _direction;
        m_impactVectorForce = 1f;
    }

    #endregion Server Methodes & RPCs
}