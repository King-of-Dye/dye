using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DestroyAfterTime : MonoBehaviour
{
    public float m_LifeDuration = 5f;

    private void Awake()
    {
        DestroyAfter();
    }

    [Server]
    private void DestroyAfter()
    {
        Destroy(this.gameObject, m_LifeDuration);
    }
}
