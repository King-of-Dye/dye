using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player Prefabs/New Player Prefabs")]
public class PlayerPrefabs : ScriptableObject
{
    public Color m_PlayerColor;
    public GameObject m_BulletPrefab = null;
    public GameObject m_PlayerDeathFX = null;
}
