using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeCard : MonoBehaviour
{
    [SerializeField] private TMP_Text m_cardName = null;
    [SerializeField] private Image m_cardIcon = null;
    [SerializeField] private TMP_Text m_cardEffectsText = null;

    public Upgrade m_CardUpgrade = null;

    [ContextMenu("Click")]
    public void UpdateCardUI()
    {
        m_cardName.text = m_CardUpgrade.m_UpgradeName;
        m_cardIcon.sprite = m_CardUpgrade.m_UpgradeIcon;

        string finalText = string.Empty;

        if(!m_CardUpgrade.m_DisplayUpgradeInfos)
        {
            finalText += m_CardUpgrade.m_AlternativeUpgradeInfos;
            m_cardEffectsText.text = finalText;
            return;
        }

        if (m_CardUpgrade.m_Health != 0)
        {
            if(m_CardUpgrade.m_Health < 0)
            {
                finalText += $"Health <color=red>{m_CardUpgrade.m_Health}</color>\n";
            }
            else
            {
                finalText += $"Health <color=green>+{m_CardUpgrade.m_Health}</color>\n";
            }
        }
        if(m_CardUpgrade.m_HealthMultiply != 0)
        {
            if(m_CardUpgrade.m_HealthMultiply < 1)
            {
                finalText += $"Health <color=red>X {m_CardUpgrade.m_HealthMultiply}</color>\n";
            }
            else
            {
                finalText += $"Health <color=green>X {m_CardUpgrade.m_HealthMultiply}</color>\n";
            }
        }
        if(m_CardUpgrade.m_HealthRegen != 0)
        {
            finalText += $"Health Regeneration <color=green>+{m_CardUpgrade.m_HealthRegen}</color>\n";
        }
        if (m_CardUpgrade.m_JumpSpeed != 0)
        {
            if (m_CardUpgrade.m_JumpSpeed < 0)
            {
                finalText += $"Jump Height <color=red>{m_CardUpgrade.m_JumpSpeed}</color>\n";
            }
            else
            {
                finalText += $"Jump Height <color=green>+{m_CardUpgrade.m_JumpSpeed}</color>\n";
            }
        }
        if (m_CardUpgrade.m_JumpSpeedMultiply != 0)
        {
            if (m_CardUpgrade.m_JumpSpeedMultiply < 1)
            {
                finalText += $"Jump Height <color=red>X {m_CardUpgrade.m_JumpSpeedMultiply}</color>\n";
            }
            else
            {
                finalText += $"Jump Height <color=green>X {m_CardUpgrade.m_JumpSpeedMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_Jumps != 0)
        {
            if(m_CardUpgrade.m_Jumps < 0)
            {
                finalText += $"Jumps <color=red>{m_CardUpgrade.m_Jumps}</color>\n";
            }
            else
            {
                finalText += $"Jumps <color=green>+{m_CardUpgrade.m_Jumps}</color>\n";
            }
        }
        if(m_CardUpgrade.m_MovementSpeed != 0)
        {
            if(m_CardUpgrade.m_MovementSpeed < 0)
            {
                finalText += $"Move Speed <color=red>{m_CardUpgrade.m_MovementSpeed}</color>\n";
            }
            else
            {
                finalText += $"Move Speed <color=green>+{m_CardUpgrade.m_MovementSpeed}</color>\n";
            }
        }
        if(m_CardUpgrade.m_MovementSpeedMultiply != 0)
        {
            if(m_CardUpgrade.m_MovementSpeedMultiply < 1)
            {
                finalText += $"Move Speed <color=red>X {m_CardUpgrade.m_MovementSpeedMultiply}</color>\n";
            }
            else
            {
                finalText += $"Move Speed <color=green>X {m_CardUpgrade.m_MovementSpeedMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_Damage != 0)
        {
            if (m_CardUpgrade.m_Damage < 0)
            {
                finalText += $"Damage <color=red>{m_CardUpgrade.m_Damage}</color>\n";
            }
            else
            {
                finalText += $"Damage <color=green>+{m_CardUpgrade.m_Damage}</color>\n";
            }
        }
        if (m_CardUpgrade.m_DamageMultiply != 0)
        {
            if (m_CardUpgrade.m_DamageMultiply < 1)
            {
                finalText += $"Damage <color=red>X {m_CardUpgrade.m_DamageMultiply}</color>\n";
            }
            else
            {
                finalText += $"Damage <color=green>X {m_CardUpgrade.m_DamageMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_SplashDamage != 0)
        {
            if (m_CardUpgrade.m_SplashDamage < 0)
            {
                finalText += $"Splash Damage <color=red>{m_CardUpgrade.m_SplashDamage}</color>\n";
            }
            else
            {
                finalText += $"Splash Damage <color=green>+{m_CardUpgrade.m_SplashDamage}</color>\n";
            }
        }
        if (m_CardUpgrade.m_SplashRadius != 0)
        {
            if (m_CardUpgrade.m_SplashRadius < 0)
            {
                finalText += $"Splash Radius <color=red>{m_CardUpgrade.m_SplashDamage}</color>m\n";
            }
            else
            {
                finalText += $"Splash Radius <color=green>+{m_CardUpgrade.m_SplashDamage}</color>m\n";
            }
        }
        if (m_CardUpgrade.m_Bullets != 0)
        {
            if (m_CardUpgrade.m_Bullets < 0)
            {
                finalText += $"Bullets <color=red>{m_CardUpgrade.m_Bullets}</color>\n";
            }
            else
            {
                finalText += $"Bullets <color=green>+{m_CardUpgrade.m_Bullets}</color>\n";
            }
        }
        if (m_CardUpgrade.m_BulletsMultiply != 0)
        {
            if (m_CardUpgrade.m_BulletsMultiply < 1)
            {
                finalText += $"Bullets <color=red>X {m_CardUpgrade.m_BulletsMultiply}</color>\n";
            }
            else
            {
                finalText += $"Bullets <color=green>X {m_CardUpgrade.m_BulletsMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_BulletSpeed != 0)
        {
            if (m_CardUpgrade.m_BulletSpeed < 0)
            {
                finalText += $"Bullets Speed <color=red>{m_CardUpgrade.m_BulletSpeed}</color>\n";
            }
            else
            {
                finalText += $"Bullets Speed <color=green>+{m_CardUpgrade.m_BulletSpeed}</color>\n";
            }
        }
        if (m_CardUpgrade.m_BulletSpeedMultiply != 0)
        {
            if (m_CardUpgrade.m_BulletSpeedMultiply < 1)
            {
                finalText += $"Bullets Speed <color=red>X {m_CardUpgrade.m_BulletSpeedMultiply}</color>\n";
            }
            else
            {
                finalText += $"Bullets Speed <color=green>X {m_CardUpgrade.m_BulletSpeedMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_BulletMass != 0)
        {
            if (m_CardUpgrade.m_BulletMass < 0)
            {
                finalText += $"Bullets Weight <color=green>{m_CardUpgrade.m_BulletMass}</color>\n";
            }
            else
            {
                finalText += $"Bullets Weight <color=red>+{m_CardUpgrade.m_BulletMass}</color>\n";
            }
        }
        if (m_CardUpgrade.m_ReloadSpeed != 0)
        {
            if (m_CardUpgrade.m_ReloadSpeed < 0)
            {
                finalText += $"Reload Speed <color=green>{m_CardUpgrade.m_ReloadSpeed}</color>\n";
            }
            else
            {
                finalText += $"Reload Speed <color=red>+{m_CardUpgrade.m_ReloadSpeed}</color>\n";
            }
        }
        if (m_CardUpgrade.m_ReloadMultiply != 0)
        {
            if (m_CardUpgrade.m_ReloadMultiply < 1)
            {
                finalText += $"Reload Speed <color=green>X {m_CardUpgrade.m_ReloadMultiply}</color>\n";
            }
            else
            {
                finalText += $"Reload Speed <color=red>X {m_CardUpgrade.m_ReloadMultiply}</color>\n";
            }
        }
        if(m_CardUpgrade.m_AttackSpeed != 0)
        {
            if(m_CardUpgrade.m_AttackSpeed < 0)
            {
                finalText += $"Attack Speed <color=green>{m_CardUpgrade.m_AttackSpeed}</color>\n";
            }
            else
            {
                finalText += $"Attack Speed <color=red>+{m_CardUpgrade.m_AttackSpeed}</color>\n";
            }
        }
        if (m_CardUpgrade.m_AttackSpeedMultiply != 0)
        {
            if (m_CardUpgrade.m_AttackSpeedMultiply < 1)
            {
                finalText += $"Attack Speed <color=green>X {m_CardUpgrade.m_AttackSpeedMultiply}</color>\n";
            }
            else
            {
                finalText += $"Attack Speed <color=red>X {m_CardUpgrade.m_AttackSpeedMultiply}</color>\n";
            }
        }
        if (m_CardUpgrade.m_BulletGravity == false)
        {
            finalText += "Bullets <color=green>float</color>";
        }

        m_cardEffectsText.text = finalText;
    }
}
