using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyUI : MonoBehaviour
{
    [SerializeField] private TMP_Text m_lobbyInfoText;
    [SerializeField] private Image m_backButtonImage;
    [SerializeField] private Image m_backButtonOutline;

    void Start()
    {
        AnimateInfoText(m_lobbyInfoText);
    }

    public void AnimateInfoText(TMP_Text _animatedText)
    {
        //m_lobbyInfoText.gameObject.transform.LeanSetLocalPosX(1400f);

        //_animatedText.gameObject.LeanMoveLocalX(-1400, 15f).setLoopClamp(10);
        _animatedText.gameObject.LeanMoveLocal(new Vector2(-1400f, 0), 13f).setLoopPingPong(-1);
    }


    public void ResetAlphaValues()
    {
        m_backButtonImage.color = new Color(1,1,1,0.55f);
        m_backButtonOutline.color = new Color(1,1,1,0);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
