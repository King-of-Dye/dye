using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndScreenUI : NetworkBehaviour
{
    [SerializeField] private TMP_Text m_winnerText = null;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    public override void OnStartServer()
    {
        NetworkManagerDye.OnServerReadied += UpdateWinnerText;
    }

    private void OnDestroy()
    {
        NetworkManagerDye.OnServerReadied -= UpdateWinnerText;
    }

    [Server]
    private void UpdateWinnerText(NetworkConnection _conn)
    {
        RpcUpdateWinnerText(_conn, Room.m_LeadingPlayer);
    }

    [TargetRpc]
    public void RpcUpdateWinnerText(NetworkConnection networkConnection, string _winnerName)
    {
        m_winnerText.text = _winnerName + " won!";
    }

    public void QuitApplication()
    {
        Application.Quit();
        Debug.Log("Quit Application");
    }
}
