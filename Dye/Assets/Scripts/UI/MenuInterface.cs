using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuInterface : MonoBehaviour
{
    [Header("Cursor")]
    [SerializeField] private Texture2D m_cursorTexture2D;

    [Header("LayoutGroups")]
    [SerializeField] private GameObject m_mainMenuGroup;
    [SerializeField] private GameObject m_optionsGroup;
    [SerializeField] private GameObject m_creditsGroup;

    [SerializeField] private Image m_logoSplashes;

    private void Start()
    {
        Cursor.SetCursor(m_cursorTexture2D, new Vector2(-1, 1), CursorMode.Auto);
        //m_logoSplashes.gameObject.LeanRotateAroundLocal(Vector3.forward, 360f, 10f).setLoopClamp(-1);
    }

    public void ChangeToOtherGroup(GameObject _group)
    {
        m_mainMenuGroup.SetActive(false);
        _group.SetActive(true);
    }

    public void ChangeToMenu(GameObject _group)
    {
        _group.SetActive(false);
        m_mainMenuGroup.SetActive(true);
    }


    public void ResetOutlineAlpha(Image _image)
    {
        _image.color = new Color(1, 1, 1, 0);
    }

    public void ResetButtonAlpha(Image _image)
    {
        _image.color = new Color(1, 1, 1, 0.55f);
    }

    public void SetFullscreen(bool _value)
    {
        Screen.fullScreen = _value;
    }

    public void ChangeToLobby()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}