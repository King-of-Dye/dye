using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailEffect : MonoBehaviour
{
    [SerializeField, Range(0.01f, 0.3f)] private float m_startWidth;
    [SerializeField, Range(0.01f, 0.1f)] private float m_endWidth;

    [SerializeField] private Camera m_mainCamera;

    [SerializeField] private TrailRenderer m_trailRenderer;

    private void Awake()
    {
        m_mainCamera = FindObjectOfType<Camera>();
        m_trailRenderer = GetComponent<TrailRenderer>();
    }

    private void Start()
    {
        SetTrailValues();
    }

    private void SetTrailValues()
    {
        m_trailRenderer.startWidth = m_startWidth;
        m_trailRenderer.endWidth = m_endWidth;
    }

    private void Update()
    {
        MoveTrailToCursor(Input.mousePosition);
        //SetTrailValues();
    }

    private void MoveTrailToCursor(Vector3 _mousePos)
    {
        transform.position = m_mainCamera.ScreenToWorldPoint(new Vector3(_mousePos.x, _mousePos.y, 5));
    }
}