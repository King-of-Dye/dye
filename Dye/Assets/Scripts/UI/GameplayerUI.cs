using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayerUI : MonoBehaviour
{

    [SerializeField] private Texture2D m_inGameCursor;

    [SerializeField] private GamePlayerDye m_player;

    public GameObject m_menuPanel;



    private string m_sceneName = "Scene_Map";

    private void Start()
    {
        m_player = GetComponentInParent<GamePlayerDye>();
    }

    private void Update()
    {
        CheckCursor();
    }

    public void RescaleButton(RectTransform _buttonTransform)
    {
        _buttonTransform.LeanScale(Vector3.one, 0.001f);
    }

    public void BackToMenu()
    {
        m_player.Disconnect();
    }

    private void CheckCursor()
    {
        Vector2 m_cursorOffset = new Vector2(32, 32);

        if(SceneManager.GetActiveScene().name.StartsWith(m_sceneName))
        {
            
            Cursor.SetCursor(m_inGameCursor, m_cursorOffset, CursorMode.Auto);
        }
    }
}
