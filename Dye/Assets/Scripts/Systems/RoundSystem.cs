using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System.Linq;
using System;

public class RoundSystem : NetworkBehaviour
{
    [SerializeField] private Animator m_animator = null;

    public List<NetworkConnection> m_RemainingPlayers = new List<NetworkConnection>();

    public static event Action OnRoundStarted;
    public static event Action OnCountdownStarted;
    public static event Action<int[]> OnPlayerScoreChanged;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    public void CountdownEnded()
    {
        m_animator.enabled = false;
    }

    public override void OnStartServer()
    {
        NetworkManagerDye.OnServerStopped += CleanUpServer;
        NetworkManagerDye.OnServerReadied += CheckToStartRound;
        PlayerRoundControllerDye.OnPlayerDeath += HandlePlayerDeath;
    }

    [ServerCallback]
    private void OnDestroy()
    {
        CleanUpServer();
    }

    [Server]
    private void CleanUpServer()
    {
        NetworkManagerDye.OnServerStopped -= CleanUpServer;
        NetworkManagerDye.OnServerReadied -= CheckToStartRound;
        PlayerRoundControllerDye.OnPlayerDeath -= HandlePlayerDeath;
    }

    [ServerCallback]
    private void StartRound()
    {
        RpcStartRound();
    }

    [Server]
    private void CheckToStartRound(NetworkConnection conn)
    {
        m_RemainingPlayers.Add(conn);

        if (Room.GamePlayers.Count(x => x.connectionToClient.isReady) != Room.GamePlayers.Count)
            return;

        m_animator.enabled = true;

        RpcStartCountdown();

        InvokePlayerScoreChangedAction();
    }

    [ClientRpc]
    private void RpcStartCountdown()
    {
        m_animator.enabled = true;

        OnCountdownStarted?.Invoke();
    }

    [ClientRpc]
    private void RpcStartRound()
    {
        Debug.Log("Round Started");

        OnRoundStarted?.Invoke();
    }

    [Server]
    public void HandlePlayerDeath(NetworkConnection conn)
    {
        if (m_RemainingPlayers.Count == 1)
            return;

        foreach (NetworkConnection player in m_RemainingPlayers)
        {
            if (player == null || player == conn)
            {
                m_RemainingPlayers.Remove(player);
                break;
            }
        }

        if (m_RemainingPlayers.Count != 1)
            return;

        StartCoroutine(HandleRoundEnd(2f));
    }

    [Server]
    private IEnumerator HandleRoundEnd(float _transitionDuration)
    {
        bool upgradePhaseInComing = false;
        bool endScreenInComing = false;

        foreach (GamePlayerDye playerDye in Room.GamePlayers)
        {
            playerDye.m_CanUpgrade = true;

            if (playerDye.netIdentity.connectionToClient == m_RemainingPlayers[0])
            {
                playerDye.m_Score++;

                if(playerDye.m_Score == 10)
                {
                    endScreenInComing = true;
                    break;
                }

                if (playerDye.m_Score % 2 == 0 && playerDye.m_Score != 0)
                {
                    playerDye.m_CanUpgrade = false;
                    upgradePhaseInComing = true;
                }
            }
        }

        InvokePlayerScoreChangedAction();

        yield return new WaitForSeconds(_transitionDuration);

        PlayerRoundControllerDye[] players = FindObjectsOfType<PlayerRoundControllerDye>();

        foreach (PlayerRoundControllerDye player in players)
        {
            player.enabled = false;
            NetworkServer.Destroy(player.gameObject);
        }

        Room.m_HighestScore = 0;
        Room.m_LeadingPlayer = "";

        foreach (GamePlayerDye playerDye in Room.GamePlayers)
        {
            if (playerDye.m_Score > Room.m_HighestScore)
            {
                Room.m_HighestScore = playerDye.m_Score;
                Room.m_LeadingPlayer = playerDye.m_DisplayName;
            }
        }

        if (endScreenInComing)
        {
            Room.GoToEndScreen();
            yield break;
        }

        if (upgradePhaseInComing)
        {
            foreach (GamePlayerDye playerDye in Room.GamePlayers)
            {
                if (playerDye.m_Score % 2 != 0)
                {
                    playerDye.m_Score--;
                }

                InvokePlayerScoreChangedAction();
            }

            Room.GoToUpgradeScene();
            yield break;
        }

        Room.GoToNextMap();
    }

    private void InvokePlayerScoreChangedAction()
    {
        int[] temp = new int[Room.numPlayers];

        for (int i = 0; i < Room.GamePlayers.Count; i++)
        {
            temp[i] = Room.GamePlayers[i].m_Score;
        }

        RpcInvokePlayerScoreChangedActionOnClient(temp);
    }

    [ClientRpc]
    public void RpcInvokePlayerScoreChangedActionOnClient(int[] _playerScores)
    {
        OnPlayerScoreChanged?.Invoke(_playerScores);
    }
}
