using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System.Linq;
using System;

public class PlayerSpawnSystem : NetworkBehaviour
{
    [SerializeField] private GameObject m_playerPrefab = null;

    public static List<Transform> m_spawnPoints = new List<Transform>();

    private int m_nextIndex = 0;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    public static void AddSpawnPoint(Transform _transform)
    {
        m_spawnPoints.Add(_transform);

        m_spawnPoints = m_spawnPoints.OrderBy(x => x.GetSiblingIndex()).ToList();
    }

    public static void RemoveSpawnPoint(Transform _transform)
    {
        m_spawnPoints.Remove(_transform);
    }

    public override void OnStartServer()
    {
        NetworkManagerDye.OnServerReadied += SpawnPlayer;
    }

    [ServerCallback]
    private void OnDestroy()
    {
        NetworkManagerDye.OnServerReadied -= SpawnPlayer;
    }

    public void SpawnPlayer(NetworkConnection conn)
    {
        Transform spawnPoint = m_spawnPoints.ElementAtOrDefault(m_nextIndex);

        if (spawnPoint == null)
        {
            Debug.LogError($"Missing spawn point for player {m_nextIndex}");
            return;
        }

        GameObject playerInstance = Instantiate(m_playerPrefab, m_spawnPoints[m_nextIndex].position, m_spawnPoints[m_nextIndex].rotation);

        foreach (GamePlayerDye playerDye in Room.GamePlayers)
        {
            if (playerDye.connectionToClient == conn)
            {
                playerInstance.GetComponent <PlayerRoundControllerDye>().m_MyGamePlayer = playerDye;
                break;
            }
        }

        NetworkServer.Spawn(playerInstance, conn);

        m_nextIndex++;
    }
}