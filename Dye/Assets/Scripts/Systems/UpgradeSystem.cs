using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System;
using TMPro;

public class UpgradeSystem : NetworkBehaviour
{
    public List<NetworkConnection> m_UpgradingPlayers = new List<NetworkConnection>();
    public List<NetworkConnection> m_PlayersInUpgradeScreen = new List<NetworkConnection>();
    public List<GamePlayerDye> m_UpgradingGamePlayers = new List<GamePlayerDye>();

    public GamePlayerDye m_MyGamePlayer = null;

    public Image m_BackgroundImage = null;
    public TMP_Text m_ChoisingPlayerText;
    public UpgradeCard[] m_UpgradeCards = new UpgradeCard[5];

    //private Camera m_camera = null;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    //private void Awake()
    //{
    //    //m_camera = FindObjectOfType<Camera>();
    //}

    public override void OnStartServer()
    {
        NetworkManagerDye.OnServerReadied += AddToPlayersList;
        GamePlayerDye.OnServerPickedUpgrade += OnServerPickedUpgradeResult;
    }

    public override void OnStartClient()
    {
        GamePlayerDye[] players = FindObjectsOfType<GamePlayerDye>();

        foreach (GamePlayerDye item in players)
        {
            if (item.hasAuthority)
            {
                m_MyGamePlayer = item;
            }
        }
    }

    private void AddToPlayersList(NetworkConnection _conn)
    {
        foreach (GamePlayerDye gamePlayerDye in Room.GamePlayers)
        {
            if (_conn == gamePlayerDye.connectionToClient)
            {
                if (gamePlayerDye.m_CanUpgrade)
                {
                    m_UpgradingPlayers.Add(_conn);
                    m_UpgradingGamePlayers.Add(gamePlayerDye);
                }
            }
        }

        m_PlayersInUpgradeScreen.Add(_conn);

        if (m_PlayersInUpgradeScreen.Count == Room.GamePlayers.Count)
        {
            NextPlayerToChooseUpgrade();
        }
    }

    private void NextPlayerToChooseUpgrade()
    {
        List<int> tempIDs = new List<int>();

        foreach (UpgradeSet set in Room.m_UpgradeSets)
        {
            if(Room.m_HighestScore >= set.m_requiredScore)
            {
                foreach (Upgrade upgrade in set.m_Upgrades)
                {
                    tempIDs.Add(upgrade.m_UpgradeID - 1);
                }
            }
        }

        //for (int i = 0; i < Room.m_UpgradeDictionary.Count; i++)
        //{
        //    tempIDs.Add(i);
        //}

        for (int i = 0; i < m_UpgradeCards.Length; i++)
        {
            int rnd = UnityEngine.Random.Range(0, tempIDs.Count);
            RpcUpdateUpgradeCards(i, tempIDs[rnd]);
            tempIDs.RemoveAt(rnd);
        }

        //for (int i = 0; i < m_UpgradeCards.Length; i++)
        //{
        //    int rnd = UnityEngine.Random.Range(1, Room.m_UpgradeDictionary.Count + 1);
        //    RpcUpdateUpgradeCards(i, rnd);
        //}

        if (m_UpgradingPlayers.Count > 0)
        {
            RpcUpdateChoisingPlayerText($"{m_UpgradingGamePlayers[0].m_DisplayName}", m_UpgradingGamePlayers[0].m_PlayerColor);
            RpcSetButtonInteractable(m_UpgradingPlayers[0], true);
        }
        else
        {
            Room.GoToNextMap();
        }
    }

    [ClientRpc]
    public void RpcUpdateChoisingPlayerText(string _playerName, Color _backgroundColor)
    {
        m_BackgroundImage.color = _backgroundColor;
        //m_camera.backgroundColor = _backgroundColor;
        m_ChoisingPlayerText.text = "Player: " + _playerName + " is choosing...";
    }

    [ClientRpc]
    public void RpcUpdateUpgradeCards(int _cardIndex, int _rnd)
    {
        m_UpgradeCards[_cardIndex].m_CardUpgrade = Room.m_UpgradeDictionary[_rnd + 1];
        m_UpgradeCards[_cardIndex].UpdateCardUI();
    }

    [TargetRpc]
    public void RpcSetButtonInteractable(NetworkConnection target, bool _buttonStatus)
    {
        ToggleButtonInteractable(_buttonStatus);
    }

    public void ToggleButtonInteractable(bool _buttonStatus)
    {
        for (int i = 0; i < m_UpgradeCards.Length; i++)
        {
            m_UpgradeCards[i].GetComponentInChildren<Button>().interactable = _buttonStatus;
        }
    }

    public void PickUpgrade(int _slotIndex)
    {
        m_MyGamePlayer.CmdPickUpgrade(_slotIndex);
    }

    [Server]
    private void OnServerPickedUpgradeResult(NetworkConnection _conn, int _slotIndex)
    {
        foreach (GamePlayerDye gamePlayer in Room.GamePlayers)
        {
            if (gamePlayer.connectionToClient == _conn)
            {
                gamePlayer.m_UpgradeIDs.Add(m_UpgradeCards[_slotIndex].m_CardUpgrade.m_UpgradeID);
                break;
            }
        }

        RpcSetButtonInteractable(m_UpgradingPlayers[0], false);

        m_UpgradingPlayers.RemoveAt(0);
        m_UpgradingGamePlayers.RemoveAt(0);

        NextPlayerToChooseUpgrade();
    }

    private void OnDestroy()
    {
        NetworkManagerDye.OnServerReadied -= AddToPlayersList;
        GamePlayerDye.OnServerPickedUpgrade -= OnServerPickedUpgradeResult;
    }
}
