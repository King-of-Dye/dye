using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using TMPro;
using UnityEngine.UI;

public class GamePlayerDye : NetworkBehaviour
{
    [SyncVar]
    public string m_DisplayName = "Loading...";

    [Header("Score and Upgrade")]
    [SyncVar] public int m_Score = 0;
    [SyncVar] public bool m_CanUpgrade = true;

    [Header("Player Stats")]
    [SyncVar] public PlayerPrefabs m_PlayerPrefabs = null;
    [SyncVar] public Color m_PlayerColor;
    [SyncVar] public List<int> m_UpgradeIDs = new List<int>();

    [SerializeField] private SoundOptions m_soundOptions;

    public static event Action<NetworkConnection, int> OnServerPickedUpgrade;

    private NetworkManagerDye room;

    private NetworkManagerDye Room
    {
        get
        {
            if (room != null)
            {
                return room;
            }

            return room = NetworkManager.singleton as NetworkManagerDye;
        }
    }

    private void Update()
    {
        CheckUIPanel();
    }

    public void HitSound()
    {
        m_soundOptions.PlayHitSound();
    }

    public override void OnStartClient()
    {
        DontDestroyOnLoad(this.gameObject);

        Room.GamePlayers.Add(this);
    }

    public override void OnStopClient()
    {
        Room.GamePlayers.Remove(this);
    }

    [Server]
    public void SetDisplayName(string _displayName)
    {
        this.m_DisplayName = _displayName;
    }

    [Server]
    public void SetPlayerColor(Color _playerColor)
    {
        this.m_PlayerColor = _playerColor;
    }

    [Server] 
    public void SetPlayerPrefabs(PlayerPrefabs _playerPrefabs)
    {
        this.m_PlayerPrefabs = _playerPrefabs;
    }

    [Command]
    public void CmdPickUpgrade(int _slotIndex)
    {
        OnServerPickedUpgrade?.Invoke(connectionToClient, _slotIndex);
    }

    #region In-Game UI

    public void Disconnect()
    {
        if(isServer)
        {
            Room.StopHost();
        }
        else
        {
            Room.StopClient();
        }
    }

    private void CheckUIPanel()
    {
        if(hasAuthority)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameObject menuPanel = GetComponentInChildren<GameplayerUI>().m_menuPanel;
                menuPanel.SetActive(!menuPanel.activeSelf);
            }
        }
    }

    public void QuitApplication()
    {
        Disconnect();
        Application.Quit();
    }
    #endregion
}
