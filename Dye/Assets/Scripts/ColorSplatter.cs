using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSplatter : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_spriteRenderer = null;
    [SerializeField] private float m_despawnDuration = 10f;
    private float m_tickRate = 0;

    private void Awake()
    {
        Destroy(this.gameObject, m_despawnDuration);
        m_tickRate = 1 / m_despawnDuration;
    }

    private void Update()
    {
        transform.position = transform.position + new Vector3(0, 0, m_tickRate * Time.deltaTime);
        m_spriteRenderer.color = new Color(m_spriteRenderer.color.r, m_spriteRenderer.color.g, m_spriteRenderer.color.b, m_spriteRenderer.color.a - (m_tickRate * Time.deltaTime));
    }
}
