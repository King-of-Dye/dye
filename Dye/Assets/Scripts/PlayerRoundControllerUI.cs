using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRoundControllerUI : MonoBehaviour
{
    [SerializeField] private Image m_HealthBar = null;

    public void UpdateHealthBar(int _maxAmount, int _currentAmount)
    {
        m_HealthBar.fillAmount = (float)_currentAmount / (float)_maxAmount;
    }
}
