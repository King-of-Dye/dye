using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : NetworkBehaviour
{
    private Rigidbody m_rigidBody;

    [SyncVar] public int m_Damage;
    [SyncVar] public float m_SplashDamage;
    [SyncVar] public float m_SplashRadius;
    [SyncVar] public float m_Mass;
    [SyncVar] public bool m_UseGravity;
    [SyncVar] public Color m_BulletColor;
    [SyncVar] public float m_BulletSpeed;
    [SyncVar] public Vector3 m_AimDirection;

    [SerializeField] private GameObject m_groundImpactFX = null;
    [SerializeField] private GameObject m_playerImpactFX = null;
    [SerializeField] private GameObject m_impactSplashSprite = null;
    private float m_impactFXDuration = 2f;

    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        InitializeBullet();
        LaunchBullet();
    }

    private void Update()
    {
        transform.right = m_rigidBody.velocity.normalized;
    }

    private void InitializeBullet()
    {
        m_rigidBody.mass = m_Mass;
        m_rigidBody.useGravity = m_UseGravity;
    }

    private void LaunchBullet()
    {
        m_rigidBody.velocity = m_AimDirection * m_BulletSpeed;
    }

    public void SetBulletVariables(float _bulletSpeed, int _damage, float _splashDamage, float _splashRadius, float _bulletMass, bool _gravity, Vector3 _aimDirection, Color _bulletColor)
    {
        this.m_BulletSpeed = _bulletSpeed;
        this.m_Damage = _damage;
        this.m_SplashDamage = _splashDamage;
        this.m_SplashRadius = _splashRadius;
        this.m_Mass = _bulletMass;
        this.m_UseGravity = _gravity;
        this.m_AimDirection = _aimDirection;
        this.m_BulletColor = _bulletColor;
    }

    [ServerCallback]
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerRoundControllerDye player = collision.gameObject.GetComponent<PlayerRoundControllerDye>();

            if (player != null)
            {
                player.TakeDamage(m_Damage);

                Vector3 impactDirection = (player.transform.position - transform.position);
                impactDirection.Normalize();

                player.AddImpactVector(impactDirection * m_Mass);
            }

            if (m_playerImpactFX != null)
            {
                GameObject instance = Instantiate(m_playerImpactFX, collision.collider.ClosestPointOnBounds(transform.position), Quaternion.identity);
                Destroy(instance.gameObject, m_impactFXDuration);
                NetworkServer.Spawn(instance);
            }
        }
        if (collision.gameObject.CompareTag("Platform"))
        {
            //Coloring Platform
            collision.gameObject.GetComponentInParent<Platform>().PlatformHit(this);

            if(m_SplashRadius > 0)
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, m_SplashRadius);

                if(colliders != null)
                {
                    foreach (Collider collider in colliders)
                    {
                        if(collider.GetComponent<PlayerRoundControllerDye>() != null)
                        {
                            collider.GetComponent<PlayerRoundControllerDye>().TakeDamage(Mathf.RoundToInt(m_Damage * m_SplashDamage));
                        }
                    }
                }
            }

            if (m_groundImpactFX != null)
            {
                GameObject instance = Instantiate(m_groundImpactFX, collision.collider.ClosestPointOnBounds(transform.position), Quaternion.identity);
                Destroy(instance.gameObject, m_impactFXDuration);
                NetworkServer.Spawn(instance);
            }

            if (m_impactSplashSprite != null)
            {
                GameObject instance = Instantiate(m_impactSplashSprite, collision.collider.ClosestPointOnBounds(transform.position) + new Vector3(0, 0, -5), Quaternion.identity);
                Destroy(instance.gameObject, 120f);
                NetworkServer.Spawn(instance);
            }
        }

        NetworkServer.Destroy(this.gameObject);
    }
}
