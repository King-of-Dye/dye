using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Upgrades/New Upgrade")]
public class Upgrade : ScriptableObject
{
    public int m_UpgradeID = 0;
    public string m_UpgradeName = string.Empty;
    public Sprite m_UpgradeIcon = null;
    public bool m_DisplayUpgradeInfos = true;
    public string m_AlternativeUpgradeInfos = "";

    [Header("Visuals")]
    public bool m_IsBodyModel = true;
    public GameObject m_UpgradeModel = null;

    [Header("Stat Changes")]
    public int m_Health = 0;
    public float m_HealthMultiply = 0;
    public int m_HealthRegen = 0;
    public float m_JumpSpeed = 0;
    public float m_JumpSpeedMultiply = 0;
    public int m_Jumps = 0;
    public float m_MovementSpeed = 0;
    public float m_MovementSpeedMultiply = 0;
    public int m_Damage = 0;
    public float m_DamageMultiply = 0;
    public float m_SplashDamage = 0;
    public float m_SplashRadius = 0;
    public int m_Bullets = 0;
    public float m_BulletsMultiply = 0;
    public float m_BulletSpeed = 0;
    public float m_BulletSpeedMultiply = 0;
    public float m_BulletMass = 0;
    public float m_ReloadSpeed = 0;
    public float m_ReloadMultiply = 0;
    public float m_AttackSpeed = 0;
    public float m_AttackSpeedMultiply = 0;

    public bool m_BulletGravity = true;
}
