using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.SceneManagement;
using Steamworks;

public class NetworkManagerDye : NetworkManager
{
    [SerializeField] private int m_minPlayers = 2;
    [Scene] [SerializeField] private string m_menuScene = string.Empty;
    [Scene] [SerializeField] private string m_lobbyScene = string.Empty;
    [Scene] [SerializeField] private string m_upgradeScene = string.Empty;
    [Scene] [SerializeField] private string m_endScene = string.Empty;

    [Header("Room")]
    [SerializeField] private RoomPlayerDye m_roomPlayerPrefab = null;
    public PlayerPrefabs[] m_PlayerPrefabs = new PlayerPrefabs[4];

    [Header("Game")]
    [SerializeField] private GamePlayerDye m_gamePlayerPrefab = null;
    [SerializeField] private GameObject m_playerSpawnSystemPrefab = null;
    [SerializeField] private GameObject m_roundSystemPrefab = null;
    [SerializeField] private GameObject m_upgradeSystemPrefab = null;
    [SerializeField] private GameObject m_playerScoreboardUIPrefab = null;

    [Header("Game Progress")]
    public int m_HighestScore = 0;
    public string m_LeadingPlayer = "";

    [Header("Maps")]
    [SerializeField] private int m_maxNumberOfRounds = 100;
    [SerializeField] private MapSet m_mapSet = null;
    private MapHandler m_mapHandler = null;

    [Header("Upgrades")]
    [SerializeField] public List<UpgradeSet> m_UpgradeSets = new List<UpgradeSet>();
    public Dictionary<int, Upgrade> m_UpgradeDictionary = new Dictionary<int, Upgrade>();

    public static event Action<NetworkConnection> OnServerReadied;
    public static event Action OnServerStopped;

    public List<RoomPlayerDye> RoomPlayers { get; } = new List<RoomPlayerDye>();
    public List<GamePlayerDye> GamePlayers { get; } = new List<GamePlayerDye>();

    public override void Awake()
    {
        base.Awake();

        foreach (UpgradeSet upgradeSet in m_UpgradeSets)
        {
            foreach (Upgrade upgrade in upgradeSet.m_Upgrades)
            {
                m_UpgradeDictionary.Add(upgrade.m_UpgradeID, upgrade);
            }
        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        if (numPlayers >= maxConnections)
        {
            conn.Disconnect();
            return;
        }

        if (SceneManager.GetActiveScene().path != m_lobbyScene)
        {
            conn.Disconnect();
            return;
        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        if (SceneManager.GetActiveScene().path == m_lobbyScene)
        {
            bool isLobbyLeader = RoomPlayers.Count == 0;

            RoomPlayerDye roomPlayerInstance = Instantiate(m_roomPlayerPrefab);

            roomPlayerInstance.IsLobbyLeader = isLobbyLeader;

            NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);

            CSteamID steamId = SteamMatchmaking.GetLobbyMemberByIndex(SteamLobby.LobbyID, numPlayers - 1);

            var roomPlayer = conn.identity.GetComponent<RoomPlayerDye>();

            roomPlayer.SetPlayerPrefabs(m_PlayerPrefabs[numPlayers - 1]);

            roomPlayer.SetSteamId(steamId.m_SteamID);
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if (conn.identity != null)
        {
            RoomPlayerDye player = conn.identity.GetComponent<RoomPlayerDye>();

            RoomPlayers.Remove(player);

            NotifyPlayersOfReadyState();
        }

        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer()
    {
        OnServerStopped?.Invoke();

        RoomPlayers.Clear();
        GamePlayers.Clear();
    }

    public void NotifyPlayersOfReadyState()
    {
        foreach (RoomPlayerDye player in RoomPlayers)
        {
            player.HandleReadyToStart(IsReadyToStart());
        }
    }

    private bool IsReadyToStart()
    {
        if (numPlayers < m_minPlayers)
            return false;

        foreach (RoomPlayerDye player in RoomPlayers)
        {
            if (!player.m_IsReady)
                return false;
        }

        return true;
    }

    public void StartGame()
    {
        //m_maxNumberOfRounds = (numPlayers * 2 * 5) - (numPlayers - 1);

        if (SceneManager.GetActiveScene().path == m_lobbyScene)
        {
            if (!IsReadyToStart())
                return;

            m_mapHandler = new MapHandler(m_mapSet, m_maxNumberOfRounds);
            ServerChangeScene(m_upgradeScene);
        }
    }

    public void GoToUpgradeScene()
    {
        ServerChangeScene(m_upgradeScene);
    }

    public void GoToEndScreen()
    {
        ServerChangeScene(m_endScene);
    }

    public void GoToNextMap()
    {
        if (m_mapHandler.m_IsComplete)
        {
            StopHost();

            return;
        }

        ServerChangeScene(m_mapHandler.m_NextMap);
    }

    public override void ServerChangeScene(string newSceneName)
    {
        if (SceneManager.GetActiveScene().path == m_lobbyScene && newSceneName.StartsWith("Assets/Scenes/Scene_Upgrade"))
        {
            for (int i = RoomPlayers.Count - 1; i >= 0; i--)
            {
                NetworkConnection conn = RoomPlayers[i].connectionToClient;
                GameObject gamePlayerInstance = Instantiate(m_gamePlayerPrefab.gameObject);
                GamePlayerDye player = gamePlayerInstance.GetComponent<GamePlayerDye>();
                player.SetPlayerPrefabs(RoomPlayers[i].m_PlayerPrefabs);
                player.SetPlayerColor(player.m_PlayerPrefabs.m_PlayerColor);
                player.SetDisplayName(RoomPlayers[i].m_DisplayName);

                NetworkServer.Destroy(conn.identity.gameObject);

                NetworkServer.ReplacePlayerForConnection(conn, gamePlayerInstance.gameObject);
            }
        }

        base.ServerChangeScene(newSceneName);
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        if (sceneName.StartsWith("Assets/Scenes/Scene_Map"))
        {
            GameObject playerSpawnSystemInstance = Instantiate(m_playerSpawnSystemPrefab);
            NetworkServer.Spawn(playerSpawnSystemInstance);

            GameObject roundSystemInstance = Instantiate(m_roundSystemPrefab);
            NetworkServer.Spawn(roundSystemInstance);

            GameObject playerScoreboardUIinstance = Instantiate(m_playerScoreboardUIPrefab);
            NetworkServer.Spawn(playerScoreboardUIinstance);
        }
        else if (sceneName.StartsWith("Assets/Scenes/Scene_Upgrade"))
        {
            GameObject upgradeSystemInstance = Instantiate(m_upgradeSystemPrefab);
            NetworkServer.Spawn(upgradeSystemInstance);

            //GameObject playerScoreboardUIinstance = Instantiate(m_playerScoreboardUIPrefab);
            //NetworkServer.Spawn(playerScoreboardUIinstance);
        }
        else if (sceneName.StartsWith("Assets/Scenes/Scene_EndScreen"))
        {
            //Endscreen Loic
        }
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);

        OnServerReadied?.Invoke(conn);
    }
}
