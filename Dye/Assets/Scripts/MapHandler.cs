using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapHandler
{
    private readonly IReadOnlyCollection<string> m_maps;
    private readonly int m_numberOfRounds;

    private int m_currentRound;
    private List<string> m_remainingMaps;

    public MapHandler(MapSet _mapSet, int _numberOfRounds)
    {
        m_maps = _mapSet.m_Maps;
        this.m_numberOfRounds = _numberOfRounds;

        ResetMaps();
    }

    public bool m_IsComplete => m_currentRound == m_numberOfRounds;

    public string m_NextMap
    {
        get
        {
            if (m_IsComplete)
                return null;

            m_currentRound++;

            if(m_remainingMaps.Count == 0)
            {
                ResetMaps();
            }

            string map = m_remainingMaps[Random.Range(0, m_remainingMaps.Count)];

            m_remainingMaps.Remove(map);

            return map;
        }
    }

    private void ResetMaps()
    {
        m_remainingMaps = m_maps.ToList();
    }
}
