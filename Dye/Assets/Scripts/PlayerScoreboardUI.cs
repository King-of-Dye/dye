using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScoreboardUI : MonoBehaviour
{
    [SerializeField] private List<PlayerScoreBoard> m_playerScoreBoards = new List<PlayerScoreBoard>();

    private void Awake()
    {
        RoundSystem.OnPlayerScoreChanged += UpdateScoreboards;
    }

    private void OnDestroy()
    {
        RoundSystem.OnPlayerScoreChanged -= UpdateScoreboards;
    }

    private void UpdateScoreboards(int[] _playerScores)
    {
        for (int i = 0; i < _playerScores.Length; i++)
        {
            m_playerScoreBoards[(_playerScores.Length - 1) - i].UpdateScoreUI(_playerScores[i]);
        }
    }
}
